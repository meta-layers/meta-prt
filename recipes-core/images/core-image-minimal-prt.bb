DESCRIPTION = "A small image just capable of allowing a device to boot plus a \
real-time test suite and tools appropriate for real-time use."

require recipes-core/images/core-image-minimal.bb

# Skip processing of this recipe if linux-intel-rt is not explicitly specified as the
# PREFERRED_PROVIDER for virtual/kernel. This avoids errors when trying
# to build multiple virtual/kernel providers.
#python () {
#    if not d.getVar("PREFERRED_PROVIDER_virtual/kernel").startswith("linux-xenomai"):
#        raise bb.parse.SkipPackage("Set PREFERRED_PROVIDER_virtual/kernel to linux-xenomai-* (as appropriate) to enable it")
#}

# standard stuff
IMAGE_INSTALL += "kernel-modules"

# further stuff needed for tests:
#IMAGE_INSTALL += "strace \
#                  rt-tests \
#                  hwlatdetect \
#                  xenomai \
#                  xenomai-demos \
#                  "



