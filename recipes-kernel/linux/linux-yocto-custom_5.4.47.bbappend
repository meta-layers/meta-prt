KTYPE="prt"

PRT_PATCH = "patch-5.4.47-rt28.patch"

# here we put the prt patches
PRTPATCHPATH="${THISDIR}/prtpatch/5.4.x"
FILESEXTRAPATHS_prepend := "${PRTPATCHPATH}:"

# prt common stuff 
SRC_URI += "file://${PRT_PATCH}"
